# MarkDown Basics

A simple repository with a README.md file to test the effects of
MarkDown formatting.

Test how this document changes when you change the formatting or
content:

1. Fork this repository
2. Edit the README.md file
3. Commit the changes
4. Check the result

Have fun!
